
package com.inkby.api.usuario.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;

@Getter
public class LoginDTO {

	private final String email;
	private final String password;
	
	public LoginDTO(@JsonProperty("email") String email, @JsonProperty("password") String password) {
		this.email = email;
		this.password = password;
	}
}

package com.inkby.api.usuario.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.inkby.api.usuario.domain.Usuario;
import com.inkby.api.usuario.dto.LoginDTO;
import com.inkby.api.usuario.exception.EmailBeenUsedException;
import com.inkby.api.usuario.exception.EmailOrSenhaInvalidException;
import com.inkby.api.usuario.repository.UsuarioRepository;
import com.inkby.api.usuario.service.IUsuarioService;

@Service
public class UsuarioService implements IUsuarioService {

	@Autowired
	private UsuarioRepository repository;
	
	@Override
	public Usuario create(Usuario usuario) {
		try {
			return repository.save(usuario);
		} catch (DataIntegrityViolationException e) {
			throw new EmailBeenUsedException(usuario.getEmail());
		}
	}

	@Override
	public Usuario login(LoginDTO login) {
		return repository.findOneByEmailAndPassword(login.getEmail(), login.getPassword())
				.orElseThrow(EmailOrSenhaInvalidException::new);
	}	
}

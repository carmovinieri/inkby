package com.inkby.api.usuario.service;

import com.inkby.api.usuario.domain.Usuario;
import com.inkby.api.usuario.dto.LoginDTO;

public interface IUsuarioService {
	public Usuario create(Usuario usuario);
	public Usuario login(LoginDTO login);
}

package com.inkby.api.usuario.exception;

public class EmailOrSenhaInvalidException extends RuntimeException {

	private static final long serialVersionUID = -5022876606649990762L;

	public EmailOrSenhaInvalidException() {
		super("Email ou senha inválido");
	}
}

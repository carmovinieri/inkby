package com.inkby.api.usuario.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inkby.api.usuario.domain.Usuario;
import com.inkby.api.usuario.dto.LoginDTO;
import com.inkby.api.usuario.service.IUsuarioService;

@CrossOrigin("*")
@RestController
@RequestMapping("usuario")
public class UsuarioController {

	@Autowired
	private IUsuarioService usuarioService;
	
	@PostMapping
	public Usuario create(@Valid @RequestBody Usuario usuario) {
		return usuarioService.create(usuario);
	}
	
	@PostMapping("/login")
	public Usuario login(@Valid @RequestBody LoginDTO login) {
		return usuarioService.login(login);
	}
}

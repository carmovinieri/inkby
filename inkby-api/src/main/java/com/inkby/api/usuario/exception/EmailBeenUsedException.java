package com.inkby.api.usuario.exception;

public class EmailBeenUsedException extends RuntimeException {

	private static final long serialVersionUID = 8519231373640711692L;
	
	public EmailBeenUsedException(String email) {
		super(String.format("O email %s já está cadastrado no sistema.", email));
	}

}

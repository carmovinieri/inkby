package com.inkby.api.loja.service;

import java.util.List;

import com.inkby.api.loja.domain.Loja;

public interface ILojaService {
	
	public List<Loja> findAll();
	public Loja findOne(Long id);
	public List<Loja> findByIdUsuario(Long idUsuario);
	public Loja create(Loja loja);
}

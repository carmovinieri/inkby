package com.inkby.api.loja.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.inkby.api.loja.domain.Loja;
import com.inkby.api.loja.service.ILojaService;

@CrossOrigin("*")
@RestController
@RequestMapping("loja")
public class LojaController {
	
	@Autowired
	private ILojaService lojaService;
	
	@GetMapping
	public List<Loja> findAll() {
		return lojaService.findAll();
	}
	
	@GetMapping("/{lojaId}")
	public Loja findOne(@PathVariable Long lojaId) {
		return lojaService.findOne(lojaId);
	}
	
	@GetMapping(params = "idUsuario")
	public List<Loja> findByIdUsuario(@RequestParam Long idUsuario) {
		return lojaService.findByIdUsuario(idUsuario);
	}
	
	@PostMapping
	public Loja create(@Valid @RequestBody Loja loja) {
		return lojaService.create(loja);
	}
}

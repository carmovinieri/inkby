package com.inkby.api.loja.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inkby.api.loja.domain.Loja;

@Repository
public interface LojaRepository extends JpaRepository<Loja, Long> {
	
	public List<Loja> findByUsuarioId(Long idUsuario);
}

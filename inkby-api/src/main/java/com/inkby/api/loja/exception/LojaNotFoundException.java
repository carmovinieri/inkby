package com.inkby.api.loja.exception;

public class LojaNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public LojaNotFoundException(Long lojaId) {
		super(String.format("Loja com id %s não foi encontrada.", lojaId));
	}
}

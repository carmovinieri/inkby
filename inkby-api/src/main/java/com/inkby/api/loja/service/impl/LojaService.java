package com.inkby.api.loja.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inkby.api.loja.domain.Loja;
import com.inkby.api.loja.exception.LojaNotFoundException;
import com.inkby.api.loja.repository.LojaRepository;
import com.inkby.api.loja.service.ILojaService;

@Service
public class LojaService implements ILojaService {

	@Autowired
	private LojaRepository repository;
	
	@Override
	public List<Loja> findAll() {
		return repository.findAll();
	}

	@Override
	public Loja findOne(Long id) {
		return repository.findById(id)
				.orElseThrow(() -> new LojaNotFoundException(id));
	}
	
	@Override
	public List<Loja> findByIdUsuario(Long idUsuario) {
		return repository.findByUsuarioId(idUsuario);
	}

	@Override
	public Loja create(Loja loja) {
		return repository.save(loja);
	}
}

package com.inkby.api.loja.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.inkby.api.usuario.domain.Usuario;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Loja implements Serializable {

	private static final long serialVersionUID = 3222693521162138442L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "O nome da loja precisa ser informado.")
	private String name;
	
	@NotNull(message = "O CNPJ da loja precisa ser informado.")
	private Long cnpj;
	
	@NotNull(message = "O tipo de conta da loja precisa ser informado.")
	@Enumerated(EnumType.STRING)
	private Account account;
	
	@ManyToOne
	private Usuario usuario;
}

package com.inkby.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InkbyApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InkbyApiApplication.class, args);
	}

}

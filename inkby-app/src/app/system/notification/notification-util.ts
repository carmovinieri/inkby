import { Injectable } from '@angular/core';

import { Notification } from './notification';
import { NotificationType } from './notification-type.enum';
import { NotificationService } from './notification.service';

const TEN_SECONDS = 10000;

@Injectable({
    providedIn: 'root'
})
export class NotificationUtil {

    constructor(private notificationService: NotificationService) {}

    public successFeedback(message: string) {
        const notification = new Notification();
        notification.message = message;
        notification.type = NotificationType.SUCCESS;
        notification.timeout = TEN_SECONDS;

        this.notificationService.add(notification);
    }

    public errorFeedback(message: string) {
        const notification = new Notification();
        notification.message = message;
        notification.type = NotificationType.DANGER;
        notification.timeout = TEN_SECONDS;
        
        this.notificationService.add(notification);
    }
}

import { NotificationType } from './notification-type.enum';

export class Notification {
    message: string;
    type: NotificationType
    timeout: number;
}

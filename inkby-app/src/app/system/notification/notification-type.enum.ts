export enum NotificationType {
    SUCCESS='success',
    INFO='info',
    WARNING='warning',
    DANGER='danger'
}

import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';
import { Notification } from './notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notifications = new Subject<Notification>();

  public notificationsAdded = this.notifications.asObservable();

  constructor() { }

  public add(notification: Notification) {
    this.notifications.next(notification);
  }
}

import { Injectable, EventEmitter } from '@angular/core';

import { Usuario } from '../shared/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioListener {

  private usuarioChanged = new EventEmitter<Usuario>();
  
  constructor() {
  }

  public emit(usuario: Usuario) {
    this.usuarioChanged.emit(usuario);
  }

  public listen(): EventEmitter<Usuario> {
    return this.usuarioChanged;
  }
}


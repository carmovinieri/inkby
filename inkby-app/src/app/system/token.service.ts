import { Injectable } from '@angular/core';

import { CookieService } from 'ngx-cookie-service';

const TOKEN_NAME = 'access_token';
const EXPIRES = 0.006944444;

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private cookieService: CookieService) { }

  saveToken(token: string) {
    this.cookieService.set(TOKEN_NAME, token, EXPIRES);
  }

  getToken(): string {
    return this.cookieService.get(TOKEN_NAME); 
  }

  isTokenSet(): boolean {
    const token = this.cookieService.get(TOKEN_NAME);
    return token !== null && token !== undefined && token !== '';
  }

  delete() {
    this.cookieService.delete(TOKEN_NAME, '/');
  }
}

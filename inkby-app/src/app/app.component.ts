import { Component, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { Usuario } from './shared/models/Usuario';
import { TokenService } from './system/token.service';
import { UsuarioListener } from './system/usuario-listener';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'inkby-app';

  usuario: Usuario;

  listenUsuarioSubscription: Subscription

  constructor(usuarioListener: UsuarioListener, private tokenService: TokenService, private router: Router) {
    if (tokenService.isTokenSet()) {
      this.usuario = JSON.parse(tokenService.getToken());
    }
    this.listenUsuarioSubscription = usuarioListener.listen().subscribe(event => this.usuario = event);
  }

  ngOnDestroy() {
    this.listenUsuarioSubscription.unsubscribe();
  }
}

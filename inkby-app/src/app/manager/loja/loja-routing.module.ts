import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LojaComponent } from './loja.component';
import { LojaFormComponent } from './loja-form/loja-form.component';


const routes: Routes = [
  { path: '', component: LojaComponent },
  { path: 'new?:account', component: LojaFormComponent },
  { path: ':lojaId', component: LojaFormComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LojaRoutingModule { }

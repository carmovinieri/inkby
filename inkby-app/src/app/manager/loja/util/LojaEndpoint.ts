import { environment } from 'src/environments/environment';

const apiURL = environment.apiUrl;
const loja = apiURL + environment.loja;

export class LojaEndpoint {

    static Loja = {
        findAll(): string {
            return loja;
        },
        findOne(lojaId: Number) {
            return `${loja}/${lojaId}`;
        },
        findByIdUsuario(idUsuario: Number) {
            return `${loja}?idUsuario=${idUsuario}`;
        },
        create(): string {
            return loja;
        } 
    };
}
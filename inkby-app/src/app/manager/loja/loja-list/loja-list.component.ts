import { Component, OnInit } from '@angular/core';

import { LojaService } from '../services/loja.service';
import { Loja } from '../models/loja';
import { Observable } from 'rxjs';
import { TokenService } from 'src/app/system/token.service';
import { Usuario } from 'src/app/shared/models/Usuario';

@Component({
  selector: 'app-loja-list',
  templateUrl: './loja-list.component.html',
  styleUrls: ['./loja-list.component.css']
})
export class LojaListComponent implements OnInit {

  lojas: Observable<Array<Loja>>;

  constructor(private lojaService: LojaService,
    private tokenService: TokenService) { }

  ngOnInit() {
    const usuario: Usuario = JSON.parse(this.tokenService.getToken());
    this.lojas = this.lojaService.findByIdUsuario(usuario.id);
  }

  lojaConfigurationURL(loja: Loja): string {
    return `/manager/loja/${loja.id}`;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { LojaRoutingModule } from './loja-routing.module';
import { LojaComponent } from './loja.component';
import { LojaFormComponent } from './loja-form/loja-form.component';
import { LojaListComponent } from './loja-list/loja-list.component';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [LojaComponent, LojaFormComponent, LojaListComponent],
  imports: [
    CommonModule,
    LojaRoutingModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot({
      validation: true
    }),
    SharedModule
  ]
})
export class LojaModule { }

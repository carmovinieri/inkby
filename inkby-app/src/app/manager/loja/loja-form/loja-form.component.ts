import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { FormValidationUtils } from 'src/app/shared/util/FormValidationUtils';
import { NotificationUtil } from 'src/app/system/notification/notification-util';
import { TokenService } from 'src/app/system/token.service';
import { Account } from '../models/account.enum';
import { Loja } from '../models/loja';
import { LojaService } from '../services/loja.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-loja-form',
  templateUrl: './loja-form.component.html',
  styleUrls: ['./loja-form.component.css']
})
export class LojaFormComponent implements OnInit {

  lojaFormGroup: FormGroup;

  submitted = false;

  btnsFormDisabled = false;

  constructor(private formBuilder: FormBuilder,
    private lojaService: LojaService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    tokenService: TokenService,
    private notificationUtil: NotificationUtil) {
    this.lojaFormGroup = this.formBuilder.group({
      id: [],
      name: ['', [Validators.required]],
      cnpj: ['', [Validators.required]],
      account: ['', [Validators.required]],
      usuario: [JSON.parse(tokenService.getToken()), Validators.required]
    });
  }

  ngOnInit() {
    const lojaId = Number(this.activatedRoute.snapshot.paramMap.get('lojaId'));
    const account = this.activatedRoute.snapshot.queryParamMap.get('account');
    
    if (lojaId) {
      this.loadLojaToEdit(lojaId);
    } else if (account) {
      this.lojaFormGroup.patchValue({
        account: Account[account]
      });
    }    
  }

  private loadLojaToEdit(lojaId: number) {
    this.lojaService.findOne(lojaId).subscribe(loja => {
      this.updateLojaFormGroupValues(loja);
    });
  }

  private updateLojaFormGroupValues(loja: Loja) {
    this.lojaFormGroup.patchValue({
      id: loja.id,
      name: loja.name,
      cnpj: loja.cnpj,
      account: Account[loja.account]
    });
  }

  get lojaFormGroupControls() {
    return this.lojaFormGroup.controls;
  }

  get isFree() {
    return this.getAccountValue() === Account.FREE;
  }

  
  get isPremium() {
    return this.getAccountValue() === Account.PREMIUM;
  }
  
  setFree() {
    this.setAccountValue(Account.FREE);
  }
  
  setPremium() {
    this.setAccountValue(Account.PREMIUM);
  }
  
  private getAccountValue() {
    return this.lojaFormGroupControls.account.value;
  }

  private setAccountValue(account: Account) {
    this.lojaFormGroupControls.account.setValue(account);
  }

  create() {
    this.submitted = true;
    if (this.lojaFormGroup.valid) {
      this.btnsFormDisabled = true;
      this.lojaService
      .create(this.lojaFormGroup.value)
      .pipe(finalize(() => this.btnsFormDisabled = false))
      .subscribe(loja => {
        this.notificationUtil.successFeedback('Tudo certo, sua loja já está disponível.');
        this.router.navigate(['/manager', 'loja']);
      }, error => this.notificationUtil.errorFeedback(error.message));
    }
  }

  applyValidationCss(field) {
    return FormValidationUtils.getCssValidation(this.submitted, this.lojaFormGroup, field);
  }
  
  clear() {
    this.lojaFormGroup.reset();
  }
}
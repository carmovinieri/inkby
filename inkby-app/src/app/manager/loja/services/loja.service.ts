import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Loja } from '../models/loja';
import { LojaEndpoint } from '../util/LojaEndpoint';
import { Observable } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class LojaService {
  
  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Array<Loja>> {
    return this.httpClient.get<Array<Loja>>(LojaEndpoint.Loja.findAll());
  }

  findOne(lojaId: Number) {
    return this.httpClient.get<Loja>(LojaEndpoint.Loja.findOne(lojaId));
  }

  findByIdUsuario(idUsuario: Number) {
    return this.httpClient.get<Array<Loja>>(LojaEndpoint.Loja.findByIdUsuario(idUsuario));
  }

  create(loja: Loja): Observable<Loja> {
    return this.httpClient.post<Loja>(LojaEndpoint.Loja.create(), loja);
  }
}

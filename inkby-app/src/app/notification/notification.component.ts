import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';

import { NotificationService } from 'src/app/system/notification/notification.service';
import { Notification } from 'src/app/system/notification/notification';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  animations: [
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ bottom: 0, opacity: 0 }),
            animate('1s ease-out', style({ bottom: 5, opacity: 1}))
          ]
        ),
        transition(
          ':leave',
          [
            style({ bottom: 5, opacity: 1}),
            animate('1s ease-in', style({ bottom: 0, opacity: 0}))
          ]
        )
      ]
    )
  ]
})
export class NotificationComponent implements OnInit {

  notification: Notification;

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.notificationsAdded.subscribe(notification => {
      this.notification = notification;     
    });
  }

  onClosed() {
    this.notification = undefined;
  }
}

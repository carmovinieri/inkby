import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Usuario } from '../../shared/models/Usuario';
import { Login } from '../models/Login';
import { UsuarioEndpoint } from '../util/UsuarioEndpoint';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private httpClient: HttpClient) { }

  create(usuario: Usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(UsuarioEndpoint.Usuario.create(), usuario);
  }

  login(login: Login): Observable<Usuario> {
    return this.httpClient.post<Usuario>(UsuarioEndpoint.Usuario.login(), login);
  }
}

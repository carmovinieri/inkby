import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FormValidationUtils } from 'src/app/shared/util/FormValidationUtils';
import { NotificationUtil } from 'src/app/system/notification/notification-util';
import { TokenService } from 'src/app/system/token.service';
import { UsuarioListener } from 'src/app/system/usuario-listener';
import { UsuarioService } from '../services/usuario.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-login-usuario',
  templateUrl: './login-usuario.component.html',
  styleUrls: ['./login-usuario.component.css']
})
export class LoginUsuarioComponent implements OnInit {

  loginFormGroup: FormGroup;

  submitted = false;

  btnsFormDisabled = false;

  constructor(formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private tokenService: TokenService,
    private usuarioListener: UsuarioListener,
    private router: Router,
    private notificationUtil: NotificationUtil) {

    this.loginFormGroup = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  ngOnInit() {
  }

  login() {
    this.submitted = true;
    if (this.loginFormGroup.valid) {
      this.btnsFormDisabled = true;
      this.usuarioService
      .login(this.loginFormGroup.value)
      .pipe(finalize(() => this.btnsFormDisabled = false))
      .subscribe(usuario => {
        this.tokenService.saveToken(JSON.stringify(usuario));
        this.usuarioListener.emit(usuario);
        this.router.navigate(['/manager', 'loja']);
      }, error => this.notificationUtil.errorFeedback(error.message));
    }
  }

  applyValidationCss(field) {
    return FormValidationUtils.getCssValidation(this.submitted, this.loginFormGroup, field);
  }

  clear() {
    this.submitted = false;
    this.loginFormGroup.reset();
  }
}
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { MustMatch } from 'src/app/shared/util/CustomValidators';
import { FormValidationUtils } from 'src/app/shared/util/FormValidationUtils';
import { NotificationUtil } from 'src/app/system/notification/notification-util';
import { UsuarioService } from '../services/usuario.service';
import { TokenService } from 'src/app/system/token.service';
import { UsuarioListener } from 'src/app/system/usuario-listener';
import { Router } from '@angular/router';




@Component({
  selector: 'app-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.css']
})
export class CadastroUsuarioComponent implements OnInit {

  usuarioFormGroup: FormGroup;

  submitted = false;
  btnsFormDisabled = false;

  constructor(formBuilder: FormBuilder,
    private usuarioService: UsuarioService,
    private notificationUtil: NotificationUtil,
    private tokenService: TokenService,
    private usuarioListener: UsuarioListener,
    private router: Router) {
    this.usuarioFormGroup = formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required, Validators.minLength(6)]]
    }, {
      validators: [
        MustMatch('password', 'Senha', 'confirmPassword')
      ]
    });
  }

  ngOnInit() {
  }

  create() {
    this.submitted = true;
    if (this.usuarioFormGroup.valid) {
      this.btnsFormDisabled = true;
      this.usuarioService
      .create(this.usuarioFormGroup.value)
      .pipe(finalize(() => this.btnsFormDisabled = false))
      .subscribe(usuario => {
        this.notificationUtil.successFeedback('Usuário cadastrado com sucesso.');
        this.tokenService.saveToken(JSON.stringify(usuario));
        this.usuarioListener.emit(usuario);
        this.router.navigate(['/manager', 'loja', 'new'], { queryParamsHandling: 'preserve'});
      }, error => this.notificationUtil.errorFeedback(error.message));
    }
  }

  clear() {
    this.submitted = false;
    this.usuarioFormGroup.reset();
  }

  applyValidationCss(field) {
    return FormValidationUtils.getCssValidation(this.submitted, this.usuarioFormGroup, field);
  }
}

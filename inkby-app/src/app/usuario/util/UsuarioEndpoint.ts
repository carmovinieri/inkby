import { environment } from 'src/environments/environment';

const apiURL = environment.apiUrl;
const usuario = apiURL + environment.usuario;


export class UsuarioEndpoint {

    static Usuario = {
        create(): string {
            return usuario;
        },
        login(): string {
            return `${usuario}/login`;
        }
    }
};
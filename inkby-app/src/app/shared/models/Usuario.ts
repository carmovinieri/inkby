export class Usuario {
    id: Number;
    name: string;
    email: string;
    password: string;
}
import { FormGroup } from '@angular/forms';

export class FormValidationUtils {
  
    static getErrorMsg(fieldName: string, validatorName: string, validatorValue?: any) { 
      const config = {
        'required': `${fieldName} é obrigatório.`,
        'email': 'Email inválido.',
        'minlength': `${fieldName} precisa ter no mínimo ${validatorValue.requiredLength} caracteres.`,
        'mustMatch': `${fieldName} precisa ser igual ao campo ${validatorValue.mustMatch}`
      };
  
      return config[validatorName];
    }

    static getCssValidation(submitted: boolean, formGroup: FormGroup, field: string) {
      return {
        'is-invalid': submitted && this.isFieldIsInvalid(formGroup, field),
        'is-valid': submitted && this.isFieldValid(formGroup, field)
      };
    }

    private static isFieldIsInvalid(formGroup: FormGroup, field: string) {
      return formGroup.get(field).invalid;
    }
  
    private static isFieldValid(formGroup: FormGroup, field: string) {
      return formGroup.get(field).valid;
    }
  }
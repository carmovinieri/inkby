import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormInvalidFeedbakComponent } from './form-invalid-feedback.component';

describe('FormInvalidFeedbakComponent', () => {
  let component: FormInvalidFeedbakComponent;
  let fixture: ComponentFixture<FormInvalidFeedbakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormInvalidFeedbakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormInvalidFeedbakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { FormValidationUtils } from '../util/FormValidationUtils';

@Component({
  selector: 'app-form-invalid-feedback',
  templateUrl: './form-invalid-feedback.component.html',
  styleUrls: ['./form-invalid-feedback.component.css'],
  
})
export class FormInvalidFeedbakComponent implements OnInit {

  @Input() showMessage: boolean;
  @Input() control: FormControl;
  @Input() label: string;

  constructor() { }

  ngOnInit() {
  }

  get errorMessage() {
    for (const propertyName in this.control.errors) {
      if (this.control.errors.hasOwnProperty(propertyName)) {
          return FormValidationUtils
          .getErrorMsg(this.label, propertyName, this.control.errors[propertyName]);
        }
    }
    return null;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormInvalidFeedbakComponent } from './form-invalid-feedback/form-invalid-feedback.component';



@NgModule({
  declarations: [FormInvalidFeedbakComponent],
  imports: [
    CommonModule
  ],
  exports: [FormInvalidFeedbakComponent]
})
export class SharedModule { }
